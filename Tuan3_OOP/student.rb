class Human
    attr_accessor :name, :gender, :hairColor, :height, :weight, :phone, :email
  
    def initialize(name, gender, height, weight, phone, email, hairColor)
      @name = name
      @gender = gender
      @height = height
      @weight = weight
      @phone = phone
      @email = email
      @hairColor = hairColor
    end
  
    def show
      puts "Tên Sinh Viên: #{name}"
      puts "Giới Tính: #{gender}"
      puts "Số điện thoại: #{phone}"
      puts "Email: #{email}"
      puts "Chiều cao: #{height}cm "
      puts "Cân nặng:#{weight}kg  " 
      puts "Màu sắc: #{hairColor}"
      puts "Số điện thoại: #{phone}"
      puts "Email: #{email}"
    end
  end
  
  class Student < Human
  
    attr_accessor :id, :_class, :school, :grade
  
    def initialize(name, gender, height, weight, phone, email, hairColor, id, _class, school, grade)
      super(name, gender, height, weight, phone, email, hairColor)
      @id = id
      @_class = _class
      @school = school
      @grade = grade
    end
  
    def show
      super
      puts "Mã Sinh Viên: #{id}"
      puts "Điểm trung bình: #{grade}"
    end
  end
  student = Student.new("Dương Trí Hùng", "Male", 172, 65, "0373257745","trihungduong.1704@gmail.com", "black", "1911505310222", "19T2", "UTE UDN", 10)
  student2 = Student.new("Huỳnh Tấn Minh", "Male", 168, 52, "0905325322","tanminh@gmail.com", "red", "1911505310236", "19T2", "UTE UDN", 5)
  student3 = Student.new("Huỳnh Thị Sâm", "Male", 153, 48, "0905325385","sam@gmail.com", "yellow", "1911505310125", "19T1", "UTE UDN", 8)

  arr = [student, student2,student3]
  puts "Điểm trung bình trước khi sắp xếp:"

  arr.each do |single|
      single.show()

  end
  
  puts "Điểm trung bình sau khi sắp xếp:"

  arr = arr.sort_by!{|a| a.grade}
  arr = arr.reverse
  i = 1
  arr.each do |s|
    puts " Top #{i}: #{s.name} : #{s.grade}"
    i+=1
  end